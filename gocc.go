/*
TODO: Add response based on status code
*/

package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/TylerBrock/colorjson"
	"github.com/alexflint/go-arg"
	"github.com/goccy/go-yaml"
)

type AboutCmd struct {
	Server string `arg:"-s,required" help:"contest server to query"`
}

type RegisterCmd struct {
	Homeserver string `arg:"-H" help:"homeserver to register on"`
	Name       string `arg:"-N,required" help:"the name of the registering user"`
	Email      string `arg:"-E,required" help:"the email of the registering user"`
	Username   string `arg:"-U" help:"the username of the registering user"`
	Password   string `arg:"-P" help:"the password for the registering user"`
}

type InfoCmd struct {
	Server  string `arg:"-s,required" help:"contest server that contains the contest"`
	Contest string `arg:"-c,required" help:"the contest to query"`
	Problem string `arg:"-p" help:"problem to query"`
}

type SolvesCmd struct {
	Server  string `arg:"-s,required" help:"contest server that contains the contest"`
	Contest string `arg:"-c,required" help:"the contest that contains the problems to find the solves for"`
	Problem string `arg:"-p" help:"problem to query"`
}

type HistoryCmd struct {
	Server  string `arg:"-s,required" help:"contest server that contains the contest"`
	Contest string `arg:"-c,required" help:"the contest that contains the submissions"`
	Problem string `arg:"-p" help:"problem to query"`
}

type SubmitCmd struct {
	Server     string `arg:"-s,required" help:"contest server to submit to"`
	Homeserver string `arg:"-H" help:"homeserver where the user is registered"`
	Contest    string `arg:"-c,required" help:"the contest that contains the problem"`
	Problem    string `arg:"-p,required" help:"the problem to solve"`
	Code       string `arg:"-c,required" help:"the file containing the code to submit"`
	Username   string `arg:"-U" help:"the user to authenticate as for the submission"`
	Password   string `arg:"-P" help:"the password for the authenticating user"`
}

type StatusCmd struct {
	Server     string `arg:"-s,required" help:"contest server where the contest to query is stored"`
	Homeserver string `arg:"-H" help:"homeserver where the user is registered"`
	Contest    string `arg:"-c,required" help:"contest to query for user"`
	Problem    string `arg:"-p" help:"problem to query"`
	Username   string `arg:"-U" help:"user's username"`
	Password   string `arg:"-P" help:"user's password"`
}

type SubmissionsCmd struct {
	Server     string `arg:"-s,required" help:"contest server where the contest to query is stored"`
	Homeserver string `arg:"-H" help:"homeserver where the user is registered"`
	Contest    string `arg:"-c,required" help:"contest to query for user"`
	Problem    string `arg:"-p" help:"problem to query"`
	Username   string `arg:"-U" help:"user's username"`
	Password   string `arg:"-P" help:"user's password"`
}

type CodeCmd struct {
	Server     string `arg:"-s,required" help:"contest server where the contest to query is stored"`
	Homeserver string `arg:"-H" help:"homeserver where the user is registered"`
	Contest    string `arg:"-c,required" help:"contest to query for user"`
	Number     string `arg:"-n,required" help:"submission number"`
	Username   string `arg:"-U" help:"user's username"`
	Password   string `arg:"-P" help:"user's password"`
}

var args struct {
	About       *AboutCmd       `arg:"subcommand:about" help:"get information about a contest server"`
	Register    *RegisterCmd    `arg:"subcommand:register" help:"register a user account to a homeserver"`
	Info        *InfoCmd        `arg:"subcommand:info" help:"information about a contest or problem"`
	Solves      *SolvesCmd      `arg:"subcommand:solves" help:"number of solves for problem or all problems in a contest"`
	History     *HistoryCmd     `arg:"subcommand:history" help:"submission history of all users in a contest"`
	Submit      *SubmitCmd      `arg:"subcommand:submit" help:"submit code for a particular problem in a contest"`
	Status      *StatusCmd      `arg:"subcommand:status" help:"query user status for a contest or problem"`
	Submissions *SubmissionsCmd `arg:"subcommand:submissions" help:"query user submissions for a contest or problem"`
	Code        *CodeCmd        `arg:"subcommand:code" help:"query user code for a contest or problem"`
	Json        bool            `arg:"-J" default:"false" help:"if used, outputs JSON instead of YAML"`
	Config      string          `arg:"-C"`
}

func authenticate(username, password, homeserver, server string) string {
	req := make(map[string]string)
	req["type"] = "authenticate"
	req["username"] = username
	req["password"] = password
	// TODO: Add support for multiple servers
	urlComponents := strings.Split(server, "://")
	req["server"] = urlComponents[len(urlComponents)-1] // FQDN without http:// or https://

	// Create JSON request body
	reqJson, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}

	// Auto-fill prefix HTTPS if none declared, leave alone if protocol exists
	if !strings.HasPrefix(homeserver, "http") {
		// Default to HTTPS
		homeserver = "https://" + strings.TrimSpace(homeserver)
	}

	// Get response token
	res, err := http.Post("https://"+homeserver, "application/json", bytes.NewBuffer(reqJson))
	if err != nil {
		log.Fatal(err)
	}

	// Read token as bytes and return string
	byteString, err := ioutil.ReadAll(res.Body)
	return strings.Trim(string(byteString), "\"" /*Remove quotes from beginning and end*/)
}

func printJson(str string) error {
	var prettifiedJson bytes.Buffer
	err := json.Indent(&prettifiedJson, []byte(str), "", "    ")
	if err != nil {
		return err
	}

	var obj map[string]interface{}
	err = json.Unmarshal([]byte(prettifiedJson.String()), &obj)
	if err != nil {
		log.Fatal(err)
	}
	f := colorjson.NewFormatter()
	f.Indent = 2

	s, _ := f.Marshal(obj)
	fmt.Println(string(s))
	return nil
}

func getUserConfig(configPath string) (username string, password string, homeserver string, err error) {
	username = ""
	password = ""
	homeserver = ""
	if configPath == "" {
		configPath, err = os.UserHomeDir()
		configPath = path.Join(configPath, ".config", "opencontest", "config")
		if err != nil {
			return "", "", "", err
		}
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		return "", "", "", nil // No data but also no error
	}

	config, err := os.Open(configPath)
	if err != nil {
		return "", "", "", err
	}
	defer func(config *os.File) {
		err := config.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(config)

	scanner := bufio.NewScanner(config)
	for scanner.Scan() {
		// Only split at first "=" sign
		// TODO: Comments in file
		// TODO: Log useless keys
		// TODO: Figure out best way to trim whitespace
		line := strings.SplitN(scanner.Text(), "=", 2)
		key := line[0]
		val := line[1]

		switch key {
		case "username":
			username = val
		case "password":
			password = val
		case "homeserver":
			homeserver = val
		}
	}

	if err := scanner.Err(); err != nil {
		return "", "", "", err
	}

	return
}

// Compares values form config file and values from flags to set username password and homeserver
func readConfigVals(confUsername string, confPassword string, confHomeserver string, argUsername string, argPassword string, argHomeserver string) (username string, password string, homeserver string, err error) {
	// TODO: Figure out best priority (probably flag should take priority)
	if confUsername != "" { // Config file takes priority
		username = confUsername
	} else if argUsername != "" {
		username = argUsername
	} else {
		err = fmt.Errorf("unable to read username! Please define it in either the configuration file or using the -U/--username flag")
	}

	if confPassword != "" { // Config file takes priority
		password = confPassword
	} else if argPassword != "" {
		password = argPassword
	} else {
		err = fmt.Errorf("unable to read password! Please define it in either the configuration file or using the -P/--password flag")
	}

	if confHomeserver != "" { // Config file takes priority
		homeserver = confHomeserver
	} else if argHomeserver != "" {
		homeserver = argHomeserver
	} else {
		err = fmt.Errorf("unable to read homeserver! Please define it in either the configuration file or using the -H/--homeserver flag")
	}

	return
}

func main() {
	// Parse arguments and create empty request to be modified based on subcommand
	arg.MustParse(&args)
	confUsername, confPassword, confHomeserver, err := getUserConfig(args.Config)
	if err != nil {
		log.Fatal(err)
	}
	req := make(map[string]string)
	// Define contestServer (location where request is made) to be defined by subcommand
	contestServer := ""
	// Switch between subcommands to define request body
	switch {
	case args.About != nil:
		req["type"] = "about"
		contestServer = args.About.Server
	case args.Register != nil:
		username, password, homeserver, err := readConfigVals(confUsername, confPassword, confHomeserver, args.Register.Username, args.Register.Password, args.Register.Homeserver)
		if err != nil {
			log.Fatal(err)
		}
		req["type"] = "register"
		req["name"] = args.Register.Name
		req["email"] = args.Register.Email
		req["username"] = username
		req["password"] = password
		contestServer = homeserver
	case args.Info != nil:
		req["type"] = "info"
		req["contest"] = args.Info.Contest
		if args.Info.Problem != "" {
			req["problem"] = args.Info.Problem
		}
		contestServer = args.Info.Server
	case args.Solves != nil:
		req["type"] = "solves"
		req["contest"] = args.Solves.Contest
		if args.Solves.Problem != "" {
			req["problem"] = args.Solves.Problem
		}
		contestServer = args.Solves.Server
	case args.History != nil:
		req["type"] = "history"
		req["contest"] = args.History.Contest
		if args.History.Problem != "" {
			req["problem"] = args.History.Problem
		}
		contestServer = args.History.Server
	case args.Submit != nil:
		username, password, homeserver, err := readConfigVals(confUsername, confPassword, confHomeserver, args.Submit.Username, args.Submit.Password, args.Submit.Homeserver)
		if err != nil {
			log.Fatal(err)
		}
		// Read code file
		code, err := ioutil.ReadFile(args.Submit.Code)
		if err != nil {
			log.Fatal(err)
		}
		req["type"] = "submit"
		req["username"] = username
		req["homeserver"] = homeserver
		// Set token equal to token returned by authenticate function
		req["token"] = authenticate(username, password, homeserver, args.Submit.Server)
		req["contest"] = args.Submit.Contest
		req["problem"] = args.Submit.Problem
		// Convert byte code read from file to string
		req["code"] = string(code)
		// Get language from file extension (TODO: use MIME types possibly)
		filenameSplit := strings.Split(args.Submit.Code, ".")
		req["language"] = filenameSplit[len(filenameSplit)-1]
		contestServer = args.Submit.Server
	case args.Status != nil:
		username, password, homeserver, err := readConfigVals(confUsername, confPassword, confHomeserver, args.Status.Username, args.Status.Password, args.Status.Homeserver)
		if err != nil {
			log.Fatal(err)
		}
		req["type"] = "status"
		req["username"] = username
		req["homeserver"] = homeserver
		req["token"] = authenticate(username, password, homeserver, args.Status.Server)
		req["contest"] = args.Status.Contest
		if args.Status.Problem != "" {
			req["problem"] = args.Status.Problem
		}
		contestServer = args.Status.Server
	case args.Submissions != nil:
		username, password, homeserver, err := readConfigVals(confUsername, confPassword, confHomeserver, args.Submissions.Username, args.Submissions.Password, args.Submissions.Homeserver)
		if err != nil {
			log.Fatal(err)
		}
		req["type"] = "submissions"
		req["username"] = username
		req["homeserver"] = homeserver
		req["token"] = authenticate(username, password, homeserver, args.Submissions.Server)
		req["contest"] = args.Submissions.Contest
		if args.Submissions.Problem != "" {
			req["problem"] = args.Submissions.Problem
		}
		contestServer = args.Submissions.Server
	case args.Code != nil:
		username, password, homeserver, err := readConfigVals(confUsername, confPassword, confHomeserver, args.Code.Username, args.Code.Password, args.Code.Homeserver)
		if err != nil {
			log.Fatal(err)
		}
		req["type"] = "code"
		req["username"] = username
		req["homeserver"] = args.Code.Homeserver
		req["token"] = authenticate(username, password, homeserver, args.Code.Server)
		req["contest"] = args.Code.Contest
		req["number"] = args.Code.Number
		contestServer = args.Code.Server
	default:
		// Not sure if this ever gets called but if something doesn't work out hopefully it returns something of a message
		fmt.Println("Invalid arguments!\nExiting...")
		os.Exit(1)
	}

	// Marshal request map into JSON
	reqJson, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}

	// POST request to contest server.
	if !strings.HasPrefix(contestServer, "http") {
		// Default to HTTPS
		contestServer = "https://" + strings.TrimSpace(contestServer)
	}
	res, err := http.Post(contestServer, "application/json", bytes.NewBuffer(reqJson))
	if err != nil {
		log.Fatal(err)
	}
	if res.StatusCode >= 400 && res.StatusCode < 600 {
		log.Fatal("Failed with error: " + res.Status) // Return full status, which is status code + short description
	}

	// Convert JSON response body into bytes to be converted in string
	byteJson, err := ioutil.ReadAll(res.Body)
	// Print JSON or YAML based on args. TODO: Add colorjson back for JSON highlighting and indenting
	if args.Json == false {
		y, err := yaml.JSONToYAML(byteJson)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(y))
	} else {
		err := printJson(string(byteJson))
		if err != nil {
			log.Fatal(err)
		}
	}
}
